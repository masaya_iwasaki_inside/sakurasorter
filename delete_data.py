import os
from utils import img_glob, get_answer_via_image_file_path

img_dir = '/Users/masayaiwasaki/Downloads/output 2'
image_path_list = img_glob(img_dir)
answers = [get_answer_via_image_file_path(p) for p in image_path_list]

counter = 0
for img_path, ans in zip(image_path_list, answers):
    if len(ans) == 0:
        print(img_path, ans)
        os.remove(img_path)
        counter += 1
    elif '●' in ans:
        print(img_path, ans)
        os.remove(img_path)
        counter += 1

print(counter)
