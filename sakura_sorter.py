# -*- coding:utf-8 -*-
import os
import re
import cv2
import argparse
import Levenshtein
from aws_handler import aws_handler
from detection_sorter import OCRDetectionSorter
from utils import img_glob, judge_category
import numpy as np

s3_detection_path = 's3://ml-ops-learning-data.inside.ai/dataset/PlantSorter/Model/OCR_Engine_detection_20180507.aiinside'
s3_all_path = 's3://ml-ops-learning-data.inside.ai/dataset/PlantSorter/Model/OCR_Engine_all_20180709.aiinside'
s3_sakura_path = 's3://ml-ops-learning-data.inside.ai/dataset/temp/sakura.zip'


def load_model(work_space_path):
    work_space_path = work_space_path
    work_space_path = os.path.join(work_space_path, 'Model')

    os.makedirs(work_space_path, exist_ok=True)
    ocr_model_path = aws_handler.download_file(s3_all_path, work_space_path)
    detection_model_path = aws_handler.download_file(s3_detection_path, work_space_path)
    return ocr_model_path, detection_model_path


def get_answer_from_image_path(image_path):
    txt_path = image_path.replace('.png', '.txt')
    answers = []
    for line in open(txt_path, 'r', encoding='shift_jis'):
        answers.append(line.replace('\n', ''))
    return answers


def sakura_sorter_with_answer(ocr_detect_sorter, image_dir, output_dir):
    os.makedirs(output_dir, exist_ok=True)
    sakura_path_list_all = sorted(img_glob(image_dir))

    load_batch_size = 128
    for data_index in range(0, len(sakura_path_list_all), load_batch_size):
        print(data_index, '/', len(sakura_path_list_all))
        sakura_path_list = sakura_path_list_all[data_index:data_index+load_batch_size]

        imgs = [cv2.imread(p, cv2.IMREAD_COLOR) for p in sakura_path_list]
        answers = [get_answer_from_image_path(p) for p in sakura_path_list]
        box_images, texts = ocr_detect_sorter.predict(imgs)
        # print(answers)
        # print(texts)

        for i in range(len(sakura_path_list)):
            box_counter = 1
            for text, img in zip(texts[i], box_images[i]):
                ans = ''
                leven_dis = 100
                for ans_ in answers[i]:
                    leven_dis_ = Levenshtein.distance(text, ans_)
                    if leven_dis_ < leven_dis:
                        leven_dis = leven_dis_
                        ans = ans_
                if len(ans) < 5 and Levenshtein.distance(ans, text) < 2 or Levenshtein.ratio(ans, text) > 0.6:
                    print(ans, text)
                    ans = ans.replace('_', '＿').replace(':', '：').replace('/', '／')
                    image_name = '{}_{:02d}_{}.png'.format(os.path.basename(sakura_path_list[i]).replace('.png', ''), box_counter, ans)
                    image_name = os.path.join(output_dir, image_name)
                    cv2.imwrite(image_name, img)
                    box_counter += 1


def sakura_sorter_without_answer(ocr_detect_sorter, image_dir, output_dir):
    os.makedirs(output_dir, exist_ok=True)
    output_dirs = {'1': os.path.join(output_dir, '1'),
                   '2': os.path.join(output_dir, '2'),
                   '3': os.path.join(output_dir, '3'),
                   '0.5': os.path.join(output_dir, '0.5'),
                   }
    for cate in output_dirs:
        os.makedirs(output_dirs[cate], exist_ok=True)

    sakura_path_list_all = sorted(img_glob(image_dir))
    load_batch_size = 128
    for data_index in range(0, len(sakura_path_list_all), load_batch_size):
        print(data_index, '/', len(sakura_path_list_all))
        sakura_path_list = sakura_path_list_all[data_index:data_index+load_batch_size]
        # print(sakura_path_list)

        imgs = [cv2.imread(p, cv2.IMREAD_COLOR) for p in sakura_path_list]
        box_images, texts = ocr_detect_sorter.predict(imgs)

        for i in range(len(sakura_path_list)):
            box_counter = 1
            median_height = np.median([_.shape[0] for _ in box_images[i]])
            # print(median_height)
            for text, img in zip(texts[i], box_images[i]):
                # print(text)
                if len(text) > 5 and 300 > img.shape[0] and 1.5 * median_height > img.shape[0]:
                    image_name = 'sakura_{}_{:02d}_{}.png'.format(os.path.basename(sakura_path_list[i]).replace('.png', ''), box_counter, text)
                    category = judge_category(text)
                    image_name = os.path.join(output_dirs[category], image_name)
                    # print(image_name)
                    cv2.imwrite(image_name, img)
                    box_counter += 1


def main():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--work_space_path', type=str, default=os.path.join(os.path.expanduser('~'), '.sakurasorter'))
    args = parser.parse_args()

    os.makedirs(args.work_space_path, exist_ok=True)
    ocr_model_path, detection_model_path = load_model(args.work_space_path)
    sakura_data_path = aws_handler.download_zip(s3_sakura_path, args.work_space_path)
    sakura_data_path = {'have_answer': os.path.join(sakura_data_path, 'sakura_free_with_ans'),
                        'no_answer': os.path.join(sakura_data_path, 'sakura_free_no_ans')}

    print('prepare sorter')
    ocr_detect_sorter = OCRDetectionSorter(ocr_model_path=ocr_model_path, detection_model_path=detection_model_path)

    # print('sorting sakura with answer')
    # sakura_sorter_with_answer(ocr_detect_sorter, sakura_data_path['have_answer'], 'output')

    print('sorting sakura without answer')
    sakura_sorter_without_answer(ocr_detect_sorter, sakura_data_path['no_answer'], 'sakura_sentence_without_answer')


if __name__ == '__main__':
    main()
