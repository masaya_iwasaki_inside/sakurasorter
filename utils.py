import os
import glob
import re
import cv2
import json
import unicodedata
import numpy as np

rep = '^[A-Za-z0-9@\*\+\-\.\(\)\$\[\]_:;<>=!?#%&×÷]+$'


def judge_category(text):
    if re.match(rep, text) is not None:
        if len(text) < 7:
            return '1'
        elif len(text) < 17:
            return '2'
    else:
        if len(text) < 7:
            return '2'
        if len(text) < 17:
            return '3'
    return '0.5'


def get_answer_via_image_file_path(image_file_path):
    json_file_path = os.path.splitext(image_file_path)[0] + '.json'
    if os.path.exists(json_file_path):
        with open(json_file_path, 'r') as f:
            json_answer = json.load(f)
            if 'answer' in list(json_answer.keys()):
                return unicodedata.normalize('NFKC', json_answer['answer'])
            elif 'verify_string' in list(json_answer.keys()):
                return unicodedata.normalize('NFKC', json_answer['verify_string'])
    return unicodedata.normalize('NFKC', os.path.splitext(os.path.basename(image_file_path))[0].split('_')[-1])


def img_glob(path):
    ext = '.*\.(jpg|jpeg|png|bmp)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def json_glob(path):
    ext = '.*\.(json)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def image_preprocessing(img, remove_top_bottom_as_well=True):
    # FIXME:batch support
    if img.shape[0] * img.shape[1] <= 0:
        return img

    # remove the redundant space on both of right and left side
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    x, y, w, h = cv2.boundingRect(np.invert(thresh))
    if remove_top_bottom_as_well:
        if w > 0 and h > 0:
            img = img[y: y + h, x: x + w, :]
    else:
        if w > 0:
            img = img[:, x: x + w, :]
    return img
